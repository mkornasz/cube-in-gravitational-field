﻿using System;
using System.Collections.Generic;
using GLfloat = System.Single;
using Southerlies.Spatial;
using SharpGL;

namespace PSW2_Jelly
{
    class Cube
    {
        public int PathLegth { get; set; }
        private bool gravitation;
        public Queue<TranslationVector> path;
        private double density, omega, delta;
        private uint[] indices;
        private double[] actualRotation;
        private GLfloat[] vertices2, normals2, baseLength;
        private TranslationVector cubeDiagonal, angle;
        private SolverRK solver;

        public Cube(bool g, double dens, double ome, int pathCount, double del, double len)
        {
            baseLength = new GLfloat[] { (float)len, (float)len, (float)len };
            gravitation = g;
            density = dens;
            omega = ome;
            delta = del;
            angle = new TranslationVector(0, 0, 0);
            path = new Queue<TranslationVector>();
            PathLegth = pathCount;
            BuildCube();
            indices = new uint[]{ 0, 1, 2,   2, 3, 0,
                       4, 5, 6,   6, 7, 4,
                       8, 9,10,  10,11, 8,
                      12,13,14,  14,15,12,
                      16,17,18,  18,19,16,
                      20,21,22,  22,23,20 };
        }

        public void BuildCube()
        {
            actualRotation = Array16FromRotation3x3(RotationMatrix.Identity);

            var vertices = new TranslationVector[]
              {
                new TranslationVector(baseLength[0],baseLength[1],baseLength[2]),
                new TranslationVector(0,baseLength[1],baseLength[2]),
                new TranslationVector(0,0,baseLength[2]),
                new TranslationVector(baseLength[0],0,baseLength[2]),

                new TranslationVector(baseLength[0],baseLength[1],baseLength[2]),
                new TranslationVector(baseLength[0],0,baseLength[2]),
                new TranslationVector(baseLength[0],0,0),
                new TranslationVector(baseLength[0],baseLength[1],0),

                new TranslationVector(baseLength[0],baseLength[1],baseLength[2]),
                new TranslationVector(baseLength[0],baseLength[1],0),
                new TranslationVector(0,baseLength[1],0),
                new TranslationVector(0,baseLength[1],baseLength[2]),

                new TranslationVector(0,baseLength[1],baseLength[2]),
                new TranslationVector(0,baseLength[1],0),
                new TranslationVector(0,0,0),
                new TranslationVector(0,0,baseLength[2]),

                new TranslationVector(0,0,0),
                new TranslationVector(baseLength[0],0,0),
                new TranslationVector(baseLength[0],0,baseLength[2]),
                new TranslationVector(0,0,baseLength[2]),

                new TranslationVector(baseLength[0],0,0),
                new TranslationVector(0,0,0),
                new TranslationVector(0,baseLength[1],0),
                new TranslationVector(baseLength[0],baseLength[1],0)
              };
            var normals = new TranslationVector[]
               {
                new TranslationVector(0,0,1),
                new TranslationVector(0,0,1),
                new TranslationVector(0,0,1),
                new TranslationVector(0,0,1),

                new TranslationVector(1,0,0),
                new TranslationVector(1,0,0),
                new TranslationVector(1,0,0),
                new TranslationVector(1,0,0),

                new TranslationVector(0,1,0),
                new TranslationVector(0,1,0),
                new TranslationVector(0,1,0),
                new TranslationVector(0,1,0),

                new TranslationVector(-1,0,0),
                new TranslationVector(-1,0,0),
                new TranslationVector(-1,0,0),
                new TranslationVector(-1,0,0),

                new TranslationVector(0,-1,0),
                new TranslationVector(0,-1,0),
                new TranslationVector(0,-1,0),
                new TranslationVector(0,-1,0),

                new TranslationVector(0,0,-1),
                new TranslationVector(0,0,-1),
                new TranslationVector(0,0,-1),
                new TranslationVector(0,0,-1)
               };
            var initialRotation = InitialRotateCube(vertices, normals);

            var x = new TranslationVector[]
          {
            new TranslationVector(0, 0, 0),
            new TranslationVector(baseLength[0], 0, 0),
            new TranslationVector(0, 0, baseLength[2]),
            new TranslationVector(baseLength[0], 0, baseLength[2]),
            new TranslationVector(0, baseLength[1], 0),
            new TranslationVector(baseLength[0], baseLength[1], 0),
            new TranslationVector(0, baseLength[1], baseLength[2]),
            new TranslationVector(baseLength[0], baseLength[1], baseLength[2])
            };
            for (int i = 0; i < x.Length; i++)
                x[i] = initialRotation * x[i];
            cubeDiagonal = x[x.Length - 1];
            var m = density * baseLength[0] * baseLength[1] * baseLength[2] / 8.0;
            RotationMatrix I = CalculateTensor(m, x);
            TranslationVector r = initialRotation * Div(new TranslationVector(baseLength[0], baseLength[1], baseLength[2]), 2.0);
            solver = new SolverRK(gravitation, r, I, omega, angle, delta);
        }

        private RotationMatrix InitialRotateCube(TranslationVector[] vertices, TranslationVector[] normals)
        {
            var aoy = Math.Atan2(baseLength[2], baseLength[0]);
            var aoz = Math.Atan2(baseLength[1], Math.Sqrt(baseLength[0] * baseLength[0] + baseLength[2] * baseLength[2]));

            var rotation = new RotationMatrix(new TranslationVector(0, 1, 0), aoy);
            rotation = new RotationMatrix(new TranslationVector(0, 0, 1), -aoz) * rotation;
            rotation = new RotationMatrix(new TranslationVector(0, 0, 1), Math.PI / 2) * rotation;

            for (int i = 0; i < vertices.Length; i++)
                vertices[i] = rotation * vertices[i];

            var cubeVertices = new List<float>();
            foreach (var v in vertices)
            {
                cubeVertices.Add((float)(v.X));
                cubeVertices.Add((float)(v.Y));
                cubeVertices.Add((float)(v.Z));
            }
            vertices2 = cubeVertices.ToArray();

            for (int i = 0; i < normals.Length; i++)
                normals[i] = rotation * normals[i];

            var cubeNormals = new List<float>();
            foreach (var v in normals)
            {
                cubeNormals.Add((float)v.X);
                cubeNormals.Add((float)v.Y);
                cubeNormals.Add((float)v.Z);
            }
            normals2 = cubeNormals.ToArray();

            return rotation;
        }

        private RotationMatrix CalculateTensor(double m, TranslationVector[] v)
        {
            double[,] M = new double[3, 3];

            for (int i = 0; i < v.Length; i++)
            {
                M[0, 0] += m * (v[i].Y * v[i].Y + v[i].Z * v[i].Z);
                M[1, 1] += m * (v[i].Z * v[i].Z + v[i].X * v[i].X);
                M[2, 2] += m * (v[i].X * v[i].X + v[i].Y * v[i].Y);
                M[0, 1] -= m * v[i].X * v[i].Y;
                M[0, 2] -= m * v[i].X * v[i].Z;
                M[1, 2] -= m * v[i].Y * v[i].Z;
            }

            M[1, 0] = M[0, 1];
            M[2, 0] = M[0, 2];
            M[2, 1] = M[1, 2];

            return new RotationMatrix(M[0, 0], M[0, 1], M[0, 2], M[1, 0], M[1, 1], M[1, 2], M[2, 0], M[2, 1], M[2, 2]);
        }

        public void GetUpdatedCube()
        {
            var rot = solver.CalculateQuaternion();
            actualRotation = Array16FromRotation3x3(rot);

            path.Enqueue(rot * cubeDiagonal);
            if (path.Count > PathLegth)
                path.Dequeue();

        }

        public void ClearPath()
        {
            path.Clear();
        }

        public void UpdateOmega(double ome)
        {
            omega = ome;
        }

        public void UpdateLength(double len)
        {
            baseLength = new GLfloat[] { (float)len, (float)len, (float)len };
        }

        public void UpdateDelta(double del)
        {
            delta = del;
        }

        public void UpdateStartAngle(double aX, double aY, double aZ)
        {
            angle = new TranslationVector(aX, aY, aZ);
        }

        public void UpdateG(bool isG)
        {
            gravitation = isG;
        }

        public void UpdateDensity(double dens)
        {
            density = dens;
        }

        public void Display(ref OpenGL gl, bool isHalfVisible)
        {
            gl.PushMatrix();
            gl.MultMatrix(actualRotation);
            if (isHalfVisible)
            {
                gl.Enable(OpenGL.GL_BLEND);     // Turn Blending On
                gl.Disable(OpenGL.GL_DEPTH_TEST);   // Turn Depth Testing Off
                gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE);

            }
            else
            {
                gl.Disable(OpenGL.GL_BLEND);
                gl.Enable(OpenGL.GL_DEPTH_TEST);
            }
            gl.EnableClientState(OpenGL.GL_VERTEX_ARRAY);
            gl.EnableClientState(OpenGL.GL_NORMAL_ARRAY);
            gl.NormalPointer(OpenGL.GL_FLOAT, 0, normals2);
            gl.VertexPointer(3, 0, vertices2);
            gl.DrawElements(OpenGL.GL_TRIANGLES, 36, indices);
            gl.DisableClientState(OpenGL.GL_VERTEX_ARRAY);
            gl.DisableClientState(OpenGL.GL_NORMAL_ARRAY);

            gl.PopMatrix();

        }

        public void DisplayCubeDiagonal(ref OpenGL gl)
        {
            gl.PushMatrix();
            gl.MultMatrix(actualRotation);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_TEXTURE);
            gl.Color(1.0f, 1.0f, 1.0f);
            gl.LineWidth(4.0f);
            gl.Begin(OpenGL.GL_LINES);
            gl.Vertex(cubeDiagonal.X, cubeDiagonal.Y, cubeDiagonal.Z);
            gl.Vertex(0, 0, 0);
            gl.End();
            gl.Enable(OpenGL.GL_LIGHTING);
            gl.Enable(OpenGL.GL_TEXTURE);
            gl.PopMatrix();

            gl.PointSize(3.0f);
        }

        public void DisplayGravitationVector(ref OpenGL gl)
        {
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_TEXTURE);
            gl.Color(1.0f, 1.0f, 1.0f);
            gl.LineWidth(3.0f);
            gl.Begin(OpenGL.GL_LINES);
            gl.Vertex(0, 0, 0);
            gl.Vertex(0, -5, 0);
            gl.End();

            gl.PointSize(8.0f);
            gl.Begin(OpenGL.GL_POINTS);
            gl.Vertex(0, -5, 0);
            gl.End();

            gl.Enable(OpenGL.GL_BLEND);
            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_SRC_ALPHA);
            gl.Color(0.8f, 0.8f, 0.8f, 0.5f);
            gl.Begin(OpenGL.GL_QUADS);
            gl.Vertex(2.0f, 0, 2.0f);
            gl.Vertex(2.0f, 0, -2.0f);
            gl.Vertex(-2.0f, 0, -2.0f);
            gl.Vertex(-2.0f, 0, 2.0f);
            gl.End();
            gl.Disable(OpenGL.GL_BLEND);

            gl.Enable(OpenGL.GL_LIGHTING);
            gl.Enable(OpenGL.GL_TEXTURE);
        }


        private TranslationVector Div(TranslationVector v, double a)
        {
            return new TranslationVector(v.X / a, v.Y / a, v.Z / a);
        }

        private GLfloat[] Mul(GLfloat[] array, float a)
        {
            GLfloat[] result = new GLfloat[array.Length];
            for (int i = 0; i < array.Length; i++)
                result[i] = array[i] * a;

            return result;
        }

        private double[] Array16FromRotation3x3(RotationMatrix rot)
        {
            return new double[]
           {
                rot[0,0],rot[1,0],rot[2,0],0,
                rot[0,1],rot[1,1],rot[2,1],0,
                rot[0,2],rot[1,2],rot[2,2],0,
                0,0,0,1
           };
        }

    }
}
