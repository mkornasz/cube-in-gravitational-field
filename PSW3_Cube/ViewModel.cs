﻿using System;
using SharpGL;
using Microsoft.Practices.Prism.Commands;
using System.Windows.Input;
using System.Windows;
using System.Windows.Threading;

namespace PSW2_Jelly
{
    class ViewModel : ViewModelBase
    {
        private Random _randomizer;
        private DispatcherTimer _simulationTimer;
        private OpenGL gl;
        private bool _isMouseDown, _isStarted = true, _isInitialized = false;
        private float _rotateHor, _rotateVer, _rotateZ, _scale;
        private double _oldMouseX, _oldMouseY, _mouseX, _mouseY, _traX, _traY;
        private bool _useGravitation, _showGravitation, _showCube, _showLine, _showPath;
        private double _omega, _dens, _length, _angleX, _angleY, _angleZ, _delta;
        private int _path;
        private Cube _cube;
        #region Propierties
        public string Length
        {
            get { return _length.ToString(); }
            set
            {
                if (_length.ToString() != value)
                {
                    if (value.Length != 0)
                        _length = double.Parse(value);
                    else
                        _length = 0;

                    OnPropertyChanged("Length");
                }
            }
        }
        public string Density
        {
            get { return _dens.ToString(); }
            set
            {
                if (_dens.ToString() != value)
                {
                    if (value.Length != 0)
                        _dens = double.Parse(value);
                    else
                        _dens = 0;
                    
                    OnPropertyChanged("Density");
                }
            }
        }
        public string Path
        {
            get { return _path.ToString(); }
            set
            {
                if (_path.ToString() != value)
                {
                    if (value.Length != 0)
                        _path = int.Parse(value);
                    else
                        _path = 0;
                    
                    OnPropertyChanged("Path");
                }
            }
        }
        public string VariationX
        {
            get { return _angleX.ToString(); }
            set
            {
                if (_angleX.ToString() != value)
                {
                    if (value.Length != 0)
                        _angleX = double.Parse(value);
                    else
                        _angleX = 0;

                    OnPropertyChanged("VariationX");
                }
            }
        }
        public string VariationY
        {
            get { return _angleY.ToString(); }
            set
            {
                if (_angleY.ToString() != value)
                {
                    if (value.Length != 0)
                        _angleY = double.Parse(value);
                    else
                        _angleY = 0;

                    OnPropertyChanged("VariationY");
                }
            }
        }
        public string VariationZ
        {
            get { return _angleZ.ToString(); }
            set
            {
                if (_angleZ.ToString() != value)
                {
                    if (value.Length != 0)
                        _angleZ = double.Parse(value);
                    else
                        _angleZ = 0;

                    OnPropertyChanged("VariationZ");
                }
            }
        }
        public string Omega
        {
            get { return _omega.ToString(); }
            set
            {
                if (_omega.ToString() != value)
                {
                    if (value.Length != 0)
                        _omega = double.Parse(value);
                    else
                        _omega = 0;
                    
                    OnPropertyChanged("Omega");
                }
            }
        }
        public string Delta
        {
            get { return _delta.ToString(); }
            set
            {
                if (_delta.ToString() != value)
                {
                    if (value.Length != 0)
                        _delta = double.Parse(value);
                    else
                        _delta = 0;

                    OnPropertyChanged("Delta");
                }
            }
        }

        public bool AnimationStop
        {
            get
            {
                return _isStarted;
            }
            set
            {
                if (_isStarted != value)
                {
                    _isStarted = value;
                    OnPropertyChanged("AnimationStop");
                }
            }
        }
        public bool UseGravitation
        {
            get
            {
                return _useGravitation;
            }
            set
            {
                if (_useGravitation != value)
                {
                    _useGravitation = value;
                    OnPropertyChanged("UseGravitation");
                }
            }
        }
        public bool ShowGravitation
        {
            get
            {
                return _showGravitation;
            }
            set
            {
                if (_showGravitation != value)
                {
                    _showGravitation = value;
                    OnPropertyChanged("ShowGravitation");
                }
            }
        }
        public bool ShowCube
        {
            get
            {
                return _showCube;
            }
            set
            {
                if (_showCube != value)
                {
                    _showCube = value;
                    OnPropertyChanged("ShowCube");
                }
            }
        }
        public bool ShowLine
        {
            get
            {
                return _showLine;
            }
            set
            {
                if (_showLine != value)
                {
                    _showLine = value;
                    OnPropertyChanged("ShowLine");
                }
            }
        }
        public bool ShowPath
        {
            get
            {
                return _showPath;
            }
            set
            {
                if (_showPath != value)
                {
                    _showPath = value;
                    OnPropertyChanged("ShowPath");
                }
            }
        }
        #endregion
        #region Commands
        private ICommand _openGLDrawExecutedCommand, _mouseDownExecutedCommand, _mouseUpExecutedCommand, _mouseMoveExecutedCommand, _mouseWheelExecutedCommand,
            _startSymExecutedCommand, _restartSymExecutedCommand, _resetExecutedCommand;
        public ICommand StartSimulation
        {
            get { return _startSymExecutedCommand ?? (_startSymExecutedCommand = new DelegateCommand(StartExecuted)); }
        }
        public ICommand StopSimulation
        {
            get { return _restartSymExecutedCommand ?? (_restartSymExecutedCommand = new DelegateCommand(StopExecuted)); }
        }
        public ICommand Reset
        {
            get { return _resetExecutedCommand ?? (_resetExecutedCommand = new DelegateCommand(ResetExecuted)); }
        }
        public ICommand DrawCommand
        {
            get { return _openGLDrawExecutedCommand ?? (_openGLDrawExecutedCommand = new DelegateCommand(OpenGLDrawExecuted)); }
        }
        public ICommand MouseDownCommand
        {
            get { return _mouseDownExecutedCommand ?? (_mouseDownExecutedCommand = new DelegateCommand(MouseDownExecuted)); }
        }
        public ICommand MouseUpCommand
        {
            get { return _mouseUpExecutedCommand ?? (_mouseUpExecutedCommand = new DelegateCommand(MouseUpExecuted)); }
        }
        public ICommand MouseMoveCommand
        {
            get { return _mouseMoveExecutedCommand ?? (_mouseMoveExecutedCommand = new DelegateCommand(MouseMoveExecuted)); }
        }
        public ICommand MouseWheelCommand
        {
            get { return _mouseWheelExecutedCommand ?? (_mouseWheelExecutedCommand = new DelegateCommand(MouseWheelExecuted)); }
        }
        private void MouseWheelExecuted()
        {
            int a = Mouse.MouseWheelDeltaForOneLine / 5;
            float numSteps = Mouse.RightButton == MouseButtonState.Pressed ? -a / 15 : a / 15;
            if (_scale + numSteps / 50 > 0)
                _scale += numSteps / 50;
        }
        private void MouseDownExecuted()
        {
            Point mouseXY = Mouse.GetPosition((Application.Current.MainWindow as MainWindow).OpenGlContr);
            _mouseX = mouseXY.X;
            _mouseY = mouseXY.Y;
            _isMouseDown = true;
        }
        private void MouseUpExecuted()
        {
            _isMouseDown = false;
        }
        private void MouseMoveExecuted()
        {
            if (!_isMouseDown)
                return;


            _oldMouseX = _mouseX;
            _oldMouseY = _mouseY;

            Point mouseXY = Mouse.GetPosition((Application.Current.MainWindow as MainWindow).OpenGlContr);
            _mouseX = mouseXY.X;
            _mouseY = mouseXY.Y;

            if (Mouse.RightButton == MouseButtonState.Pressed)
            {
                _traX += (_mouseX - _oldMouseX) / 100;
                _traY += -(_mouseY - _oldMouseY) / 100;
            }
            else
            {
                if ((_mouseX - _oldMouseX) > 0)
                    _rotateHor += 3.0f;
                else if ((_mouseX - _oldMouseX) < 0)
                    _rotateHor -= 3.0f;

                if (Keyboard.IsKeyDown(Key.Z))
                {
                    if ((_mouseY - _oldMouseY) > 0)
                        _rotateZ += 3.0f;
                    else if ((_mouseY - _oldMouseY) < 0)
                        _rotateZ -= 3.0f;
                }
                else
                {
                    if ((_mouseY - _oldMouseY) > 0)
                        _rotateVer += 3.0f;
                    else if ((_mouseY - _oldMouseY) < 0)
                        _rotateVer -= 3.0f;
                }

            }
        }

        private void StartExecuted()
        {
            AnimationStop = false;
            ResetExecuted();
            _simulationTimer.Start();
        }
        private void ResetExecuted()
        {
            _cube.PathLegth = _path;
            _cube.UpdateG(UseGravitation);
            _cube.UpdateDensity(_dens);
            _cube.UpdateLength(_length);
            _cube.UpdateOmega(_omega);
            _cube.UpdateStartAngle(_angleX,_angleY,_angleZ);
            _cube.UpdateDelta(_delta);
            _cube.ClearPath();
            _cube.BuildCube();
        }
        private void StopExecuted()
        {
            _simulationTimer.Stop();
            AnimationStop = true;
        }

        #endregion

        public ViewModel()
        {
            gl = new OpenGL();
            _randomizer = new Random();
            InitializeVariables();
            _simulationTimer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, 1) };
            _simulationTimer.Tick += simulationTick;
        }


        private void OpenGLDrawExecuted()
        {
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(_traX, _traY, -3.0);
            gl.Rotate(_rotateVer, _rotateHor, _rotateZ);
            gl.Scale(_scale, _scale, _scale);


            if (!_isInitialized)
            {
                gl.Enable(OpenGL.GL_DEPTH_TEST);

                float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
                float[] light0pos = new float[] { 7.0f, 2.0f, 5.0f, 1.0f };
                float[] light0ambient = new float[] { 0.0f, 1.0f, 0.4f, 1.0f };
                float[] light0diffuse = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
                float[] light1diffuse = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
                float[] light0specular = new float[] { 0.1f, 0.1f, 0.1f, 1.0f };

                float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };

                gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);

                gl.Enable(OpenGL.GL_LIGHTING);
                gl.Enable(OpenGL.GL_LIGHT0);
                gl.ShadeModel(OpenGL.GL_SMOOTH);
                _isInitialized = true;


            }
            DisplayAxes();
            if (ShowCube)
                _cube.Display(ref gl, ShowLine);
            if (ShowLine)
                _cube.DisplayCubeDiagonal(ref gl);
            if (ShowPath)
                DisplayPath();
            if (ShowGravitation)
                _cube.DisplayGravitationVector(ref gl);
        }
        private void DisplayAxes()
        {
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_TEXTURE);
            gl.Color(1.0f, 0.0f, 0.0f);
            gl.LineWidth(0.01f);
            gl.Begin(OpenGL.GL_LINES);
            gl.Vertex(-10, 0.0, 0.0);
            gl.Vertex(10, 0, 0);
            gl.Color(0.0f, 1.0f, 0.0f);
            gl.Vertex(0.0, 0.0, -10);
            gl.Vertex(0.0, 0.0, 10);
            gl.Color(0.0f, 0.0f, 1.0f);
            gl.Vertex(0.0, -10.0, 0.0);
            gl.Vertex(0.0, 10.0, 0.0);
            gl.End();
            gl.Enable(OpenGL.GL_LIGHTING);
            gl.Enable(OpenGL.GL_TEXTURE);
        }

        private void InitializeVariables()
        {
            _scale = 0.6f;
            _traY = _traX = 0.0f;
            _mouseX = _mouseY = 0;
            _rotateZ = _rotateVer = 0.0f;
            _rotateHor = 5.0f;
            _isMouseDown = false;
            _showCube = _useGravitation = true;
            _showGravitation = _showLine = _showPath = false;
            _angleY = _angleZ = _angleX = 0;
            _dens = 10;
            _length = 1;
            _omega = 0.0;
            _delta = 0.01;
            _path = 500;
            _cube = new Cube(UseGravitation, _dens, _omega, _path,_delta, _length);
        }

        private void simulationTick(object sender, EventArgs e)
        {
            _cube.GetUpdatedCube();
        }

        private void DisplayPath()
        {
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_TEXTURE);
            gl.Color(0.0f, 1.0f, 1.0f);
            gl.LineWidth(2.0f);
            gl.Begin(OpenGL.GL_LINES);
            var p = _cube.path.ToArray();
            for (int i = 1; i < _cube.path.Count; i++)
            {
                gl.Vertex(p[i - 1].X, p[i - 1].Y, p[i - 1].Z);
                gl.Vertex(p[i].X, p[i].Y, p[i].Z);
            }
            gl.End();
            gl.Enable(OpenGL.GL_LIGHTING);
            gl.Enable(OpenGL.GL_TEXTURE);
        }
    }
}
