﻿using System;
using Southerlies.Spatial;
using Accord.Math;

namespace PSW2_Jelly
{
    public class SolverRK
    {
        #region Fields
        private bool gravitation;
        private double omega, delta;
        private double[] Qoryg;
        private RotationMatrix I, IInv;
        private TranslationVector angle, r, N, g = new TranslationVector(0, -9.81, 0);
        public TranslationVector GravitationVector { get { return g; } }
        #endregion

        public SolverRK(bool isGravitation, TranslationVector rr, RotationMatrix tensor, double ome, TranslationVector an, double del)
        {
            r = rr;
            omega = ome;
            delta = del;
            gravitation = isGravitation;
            angle = UpdateA(an);
            I = tensor;
            IInv = Invert(tensor);
            var mat = new RotationMatrix(new TranslationVector(1, 0, 0), angle.X);
            mat = new RotationMatrix(new TranslationVector(0, 1, 0), angle.Y) * mat;
            mat = new RotationMatrix(new TranslationVector(0, 0, 1), angle.Z) * mat;
            var q = mat.GetAsQuaternion().GetNormalized();
            Qoryg = new double[] { 0, omega, 0, q.X, q.Y, q.Z, q.W };
        }

        public TranslationVector UpdateA(TranslationVector a)
        {
            return new TranslationVector(a.X * Math.PI / 180.0, a.Y * Math.PI / 180.0, a.Z * Math.PI / 180.0);
        }

        private double[] CalculateFunction(double[] Q)
        {
            TranslationVector w = new TranslationVector(Q[0], Q[1], Q[2]);
            Quaternion q = MakeQuaternion(Q[3], Q[4], Q[5], Q[6]);
            q.Normalize();
            var qm = q.GetAsMatrix().GetInverted();

            N = gravitation ? r.Cross(qm * g) : new TranslationVector(0, 0, 0);

            var Wt = IInv * (N + (I * w).Cross(w));
            var qw = MakeQuaternion(w.X, w.Y, w.Z, 0.0);
            var Qt = Div(q * qw, 2.0);

            return new double[7] { Wt.X, Wt.Y, Wt.Z, Qt.X, Qt.Y, Qt.Z, Qt.W };
        }

        private double[] RungeKutta(double[] y)
        {
            double h = delta;
            double[] k1 = CalculateFunction(y);
            double[] k2 = CalculateFunction(Add(y, Mul(k1, h / 2)));
            double[] k3 = CalculateFunction(Add(y, Mul(k2, h / 2)));
            double[] k4 = CalculateFunction(Add(y, Mul(k3, h)));

            double[] result = new double[k1.Length];
            for (int i = 0; i < result.Length; i++)
                result[i] = y[i] + (k1[i] + 2 * k2[i] + 2 * k3[i] + k4[i]) * h / 6;

            return result;
        }

        #region HelpfulMethod

        private Quaternion MakeQuaternion(double x, double y, double z, double w)
        {
            Quaternion q = new Quaternion();
            q[0] = x;
            q[1] = y;
            q[2] = z;
            q[3] = w;

            return q;
        }

        public RotationMatrix CalculateQuaternion()
        {
            Qoryg = RungeKutta(Qoryg);
            Quaternion q = MakeQuaternion(Qoryg[3], Qoryg[4], Qoryg[5], Qoryg[6]);
            return q.GetAsMatrix();
        }

        public RotationMatrix Invert(RotationMatrix m)
        {
            double[,] mat = new double[,] { { m[0, 0], m[0, 1], m[0, 2] }, { m[1, 0], m[1, 1], m[2, 1] }, { m[2, 0], m[2, 1], m[2, 2] } };
            double[,] inverse = Matrix.Inverse(mat);
            return new RotationMatrix(inverse[0, 0], inverse[0, 1], inverse[0, 2], inverse[1, 0], inverse[1, 1], inverse[2, 1], inverse[2, 0], inverse[2, 1], inverse[2, 2]);
        }

        private double[] Mul(double[] array, double a)
        {
            double[] result = new double[array.Length];
            for (int i = 0; i < array.Length; i++)
                result[i] = array[i] * a;

            return result;
        }

        private double[] Add(double[] array, double[] array2)
        {
            double[] result = new double[array.Length];
            for (int i = 0; i < array.Length; i++)
                result[i] = array[i] + array2[i];

            return result;
        }

        private Quaternion Div(Quaternion q, double v)
        {
            return MakeQuaternion(q.X / v, q.Y / v, q.Z / v, q.W / v);
        }
        #endregion
    }
}
